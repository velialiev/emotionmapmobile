import React from 'react'
import theme from '../../configs/theme'
import { ScrollView, StyleSheet, View } from 'react-native'
import Post from '../PostsScreen/Post'

const PostsByEmotionScreen = () => {
  return (
    <>
      <View style={{ ...theme.defaultScreenStyles, ...styles.screenWrap }}>
        <ScrollView>
          <Post/>
          <Post/>
          <Post/>
          <Post/>
        </ScrollView>
      </View>
    </>
  )
}

export default PostsByEmotionScreen

const styles = StyleSheet.create({
  screenWrap: {
    flexDirection: 'column',
    paddingHorizontal: 15,
    paddingVertical: 15,
  },
})

import React from 'react'
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
// import loveIcon from '../../assets/love.png'/
import theme from '../../configs/theme'

const ChoiceEmotionScreen = ({ route, navigation }: any) => {
  const { emoji, currentEmotion } = route.params

  return (
    <View style={{ ...theme.defaultScreenStyles, ...styles.screenWrap }}>
      {emoji.map(({ id, name, icon }) => {
        return (
          <TouchableOpacity
            onPress={() =>
              navigation.navigate('PostCreationScreen', { currentEmotion: { id, name } })
            }
            style={styles.emojWrap}
          >
            <View
              key={id}
              style={{
                ...styles.iconWrap,
                opacity: id === currentEmotion?.id ? 1 : 0.52,
              }}
            >
              <Image source={icon} width={25} height={25} style={{ width: 35, height: 35 }}/>
            </View>
            <Text
              style={{
                color: id === currentEmotion?.id ? '#000' : '#99999',
              }}
            >
              {name}
            </Text>
          </TouchableOpacity>
        )
      })}
    </View>
  )
}

export default ChoiceEmotionScreen

const styles = StyleSheet.create({
  screenWrap: {
    padding: 20,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  iconWrap: {
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 10,
    backgroundColor: 'white',
    width: 70,
    height: 70,
    borderRadius: 100,
    marginBottom: 10,
    borderWidth: 0.5,
    borderColor: '#cbcaca',
  },
  emojWrap: {
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 15,
    marginBottom: 15,
  },
})

import React from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'
import avatar from '../../../assets/avatar.jpg'
import auto from '../../../assets/auto.jpg'

import like from '../../../assets/like.png'
import share from '../../../assets/share.png'
import views from '../../../assets/views.png'

const Post = () => {
  return (
    <View style={styles.cardWrap}>
      <View style={styles.cardHeader}>
        <Image source={avatar} style={{ width: 50, height: 50, borderRadius: 50 }} />
        <Text style={styles.headerTitle}>Али Алиев</Text>
      </View>

      <View>
        <Image source={auto} style={{ width: '100%', height: 200 }} />
      </View>

      <View style={styles.footer}>
        <Text style={styles.footerText}>16 комментариев</Text>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <View style={{ flexDirection: 'row' }}>
            <Image source={like} />
            <Image source={share} />
          </View>
          <Image source={views} />
        </View>
      </View>
    </View>
  )
}

export default Post

const styles = StyleSheet.create({
  cardWrap: {
    borderWidth: 0.5,
    borderRadius: 10,
    borderColor: '#99a2ad',
    marginBottom: 15,
  },
  cardHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingVertical: 10,
  },
  headerTitle: {
    fontWeight: 'bold',
    fontSize: 18,
    marginLeft: 15,
  },
  footer: {
    flexDirection: 'column',
  },
  footerText: {
    paddingHorizontal: 10,
    paddingVertical: 10,
    color: '#99a2ad',
    fontSize: 16,
  },
})

import React from 'react'
import { Image, ScrollView, StyleSheet, TouchableOpacity, View } from 'react-native'
import theme from '../../configs/theme'
import Post from './Post'
import maps from '../../assets/map.png'

const PostsScreen = ({navigation}: any) => {
  return (
    <View style={{ ...theme.defaultScreenStyles, ...styles.screenWrap }}>
      <ScrollView>
        <Post />
        <Post />
        <Post />
        <Post />
      </ScrollView>
      <TouchableOpacity
        style={{ position: 'absolute', bottom: 15, right: 15 }}
        onPress={() => navigation.navigate('EmotionMapScreen')}>
        <Image source={maps} style={{ width: 44, height: 44 }} />
      </TouchableOpacity>
    </View>
  )
}

export default PostsScreen

const styles = StyleSheet.create({
  screenWrap: {
    flexDirection: 'column',
    paddingHorizontal: 15,
    paddingVertical: 15,
  },
})

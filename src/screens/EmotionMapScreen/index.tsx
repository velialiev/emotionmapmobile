import React, { FC, useCallback, useMemo, useState } from 'react'
import { Image, StyleSheet, View } from 'react-native'
import theme from '../../configs/theme'
import { NavigationScreenProp } from 'react-navigation'
import FilterBar from './FilterBar'
import MapboxGL from '@react-native-mapbox-gl/maps'
import loveIcon from '../../assets/emoji/love.png'
import foolIcon from '../../assets/emoji/fool.png'
import happyIcon from '../../assets/emoji/happy.png'
import joyIcon from '../../assets/emoji/joy.png'
import embraceIcon from '../../assets/emoji/embrace.png'
import shynessIcon from '../../assets/emoji/shyness.png'
import smileIcon from '../../assets/emoji/smile.png'

MapboxGL.setAccessToken(
  'pk.eyJ1IjoiaGFyb2RpZXIxMCIsImEiOiJja2Y4ZnFuZnEwM2p6MzJsZGdqOHFqbjlwIn0.BquFfL4AvZzVkLEJu5Gwsg',
)
console.disableYellowBox = true

const emotionCoordinates = [
  { id: 1, emotionName: 'Любовь', icon: loveIcon, coordinates: [34.06, 44.94] },
  { id: 2, emotionName: 'Любовь', icon: loveIcon, coordinates: [34.08, 44.95] },
  { id: 3, emotionName: 'Любовь', icon: loveIcon, coordinates: [34.14, 44.93] },
  { id: 4, emotionName: 'Дурачимся', icon: foolIcon, coordinates: [34.09, 44.92] },
  { id: 5, emotionName: 'Дурачимся', icon: foolIcon, coordinates: [34.12, 44.97] },
  { id: 6, emotionName: 'Веселимся', icon: happyIcon, coordinates: [34.07, 44.92] },
  { id: 7, emotionName: 'Радость', icon: joyIcon, coordinates: [34.07, 44.98] },
  { id: 8, emotionName: 'Радость', icon: joyIcon, coordinates: [34.15, 44.98] },
  { id: 9, emotionName: 'Обнимашки', icon: embraceIcon, coordinates: [34.1, 44.98] },
  { id: 10, emotionName: 'Неловкость', icon: shynessIcon, coordinates: [34.1, 44.96] },
  { id: 11, emotionName: 'Неловкость', icon: shynessIcon, coordinates: [34.05, 44.89] },
  { id: 12, emotionName: 'Счастье', icon: smileIcon, coordinates: [34.13, 44.94] },
]

export type Emotion = typeof emotionCoordinates[0]

const EmotionMapScreen: FC<Props> = ({ navigation }) => {
  const [activeEmojiFilters, setActiveEmojiFilters] = useState<Emotion['emotionName'][]>([])

  const isFilterActive = useCallback(
    (emotionName: Emotion['emotionName']) => {
      return activeEmojiFilters.includes(emotionName)
    },
    [activeEmojiFilters],
  )

  const selectFilter = (emotionName: Emotion['emotionName']) => {
    console.log(activeEmojiFilters)

    if (isFilterActive(emotionName)) {
      setActiveEmojiFilters(activeEmojiFilters.filter((emojiId) => emojiId !== emotionName))
      return
    }

    setActiveEmojiFilters([...activeEmojiFilters, emotionName])
  }

  const filteredEmojis = useMemo(() => {
    if (activeEmojiFilters.length === 0) {
      return emotionCoordinates
    }

    return emotionCoordinates.filter((emotionCoordinates) =>
      isFilterActive(emotionCoordinates.emotionName),
    )
  }, [activeEmojiFilters.length, isFilterActive])

  const coordinates = [34.1, 44.94]
  return (
    <View style={{ ...theme.defaultScreenStyles }}>
      <MapboxGL.MapView style={{ flex: 1 }}>
        <MapboxGL.Camera zoomLevel={12} centerCoordinate={coordinates} />

        {filteredEmojis.map(({ id, icon, coordinates, emotionName }) => {
          return (
            <MapboxGL.PointAnnotation
              key={id.toString()}
              coordinate={coordinates}
              id={id.toString()}
              onSelected={() =>
                navigation.navigate('PostsByEmotionScreen', {
                  emotion: { id, icon, coordinates, emotionName },
                })
              }
            >
              <Image source={icon} style={{ width: 30, height: 30 }} />
            </MapboxGL.PointAnnotation>
          )
        })}
      </MapboxGL.MapView>
      <View style={{ position: 'absolute', bottom: 0, width: '100%' }}>
        <FilterBar onFiltersChange={selectFilter} activeEmojiFilters={activeEmojiFilters} />
      </View>
    </View>
  )
}

export default EmotionMapScreen

const styles = StyleSheet.create({})

interface Props {
  navigation: NavigationScreenProp<any, any>
}

import React, { FC, useMemo, useState } from 'react'
import { Image, ScrollView, StyleSheet, Text, TouchableOpacity, View, } from 'react-native'
import Input from '../../../components/Input'
import { Formik } from 'formik'
import { emoji } from '../../PostCreationScreen'
import { Emotion } from '../index'

const FilterBar: FC<Props> = ({ onFiltersChange, activeEmojiFilters }) => {
  const [search, setSearch] = useState('')

  const isFilterActive = (emotionName: string) => {
    return activeEmojiFilters.includes(emotionName)
  }

  const searchedEmojis = useMemo(() => {
    return emoji.filter((emoji) => emoji.name.toLowerCase().includes(search.toLowerCase()))
  }, [emoji, search])

  return (
    <View style={styles.wrap}>
      <Formik
        onSubmit={() => console.log()}
        initialValues={{
          search: '',
        }}
      >
        <Input
          name="search"
          onChange={(e: any) => setSearch(e.nativeEvent.text)}
          placeholder="Поиск по настроениям"
        />
      </Formik>
      <View style={styles.footer}>
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          {searchedEmojis?.length ? (
            searchedEmojis.map(({ id, name, icon }) => {
              return (
                <TouchableOpacity
                  onPress={() => {
                    onFiltersChange(name)
                  }}
                  style={styles.emojWrap}
                >
                  <View
                    key={id}
                    style={{
                      ...styles.iconWrap,
                      opacity: isFilterActive(name) ? 1 : 0.52,
                    }}
                  >
                    <Image source={icon} width={25} height={25} style={{ width: 35, height: 35 }} />
                  </View>
                  <Text
                    style={{
                      color: isFilterActive(name) ? '#000' : '#999',
                    }}
                  >
                    {name}
                  </Text>
                </TouchableOpacity>
              )
            })
          ) : (
            <Text>Нет результатов :(</Text>
          )}
        </ScrollView>
      </View>
    </View>
  )
}

export default FilterBar

interface Props {
  onFiltersChange: (name: string) => void
  activeEmojiFilters: Emotion['emotionName'][]
}

const styles = StyleSheet.create({
  wrap: {
    paddingHorizontal: 20,
    paddingTop: 20,
    borderWidth: 1,
    elevation: 0.5,
    borderTopEndRadius: 20,
    borderTopStartRadius: 20,
    borderColor: '#E1E3E6',
    backgroundColor: 'white',
  },
  footer: {
    height: 120,
    flexDirection: 'row',
    marginTop: 20,
  },
  iconWrap: {
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 10,
    backgroundColor: 'white',
    width: 70,
    height: 70,
    borderRadius: 100,
    marginBottom: 10,
  },
  emojWrap: {
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 15,
    marginBottom: 15,
  },
})

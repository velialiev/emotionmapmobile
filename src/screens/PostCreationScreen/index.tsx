import React, { FC } from 'react'
import { Image, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View, } from 'react-native'
import { NavigationScreenProp } from 'react-navigation'
import theme from '../../configs/theme'
import check from '../../assets/check.png'
import smile from '../../assets/smile.png'
import user from '../../assets/user.png'
import time from '../../assets/time.png'

import loveIcon from '../../assets/emoji/love.png'
import foolIcon from '../../assets/emoji/fool.png'
import happyIcon from '../../assets/emoji/happy.png'
import joyIcon from '../../assets/emoji/joy.png'
import embraceIcon from '../../assets/emoji/embrace.png'
import shynessIcon from '../../assets/emoji/shyness.png'
import smileIcon from '../../assets/emoji/smile.png'

const selects = [
  { id: 1, text: 'Видно всем', icon: user },
  { id: 2, text: 'Сейчас', icon: time },
]

export const emoji: Emoji[] = [
  { id: 0, name: 'Любовь', icon: loveIcon },
  { id: 1, name: 'Дурачимся', icon: foolIcon },
  { id: 2, name: 'Веселимся', icon: happyIcon },
  { id: 3, name: 'Радость', icon: joyIcon },
  { id: 4, name: 'Обнимашки', icon: embraceIcon },
  { id: 5, name: 'Неловкость', icon: shynessIcon },
  { id: 6, name: 'Счастье', icon: smileIcon },
]

const getEmotionById = (id: number) => emoji.find((e) => e.id === id)

const PostCreationScreen: FC<Props> = ({ navigation, route }) => {
  const currentEmotion = route.params?.currentEmotion

  return (
    <View style={{ ...theme.defaultScreenStyles }}>
      <TextInput
        multiline={true}
        placeholder="Что у Вас нового?"
        style={{ fontSize: 20, paddingHorizontal: 15, paddingVertical: 15 }}
      />
      <View style={styles.selectsRowWrap}>
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          <TouchableOpacity
            onPress={() => navigation.navigate('ChoiceEmotionScreen', { emoji, currentEmotion })}
            style={currentEmotion ? styles.selectActive : styles.select}
          >
            <Image
              source={!currentEmotion ? smile : getEmotionById(currentEmotion.id)?.icon}
              style={{ marginRight: 8, width: 20, height: 20 }}
              width={20}
              height={20}
            />

            {!currentEmotion ? (
              <Text style={styles.selectText}>Настроение</Text>
            ) : (
              <Text style={styles.selectText}>{route.params?.currentEmotion.name}</Text>
            )}

            <Image source={check} style={{ marginLeft: 8 }} />
          </TouchableOpacity>

          {selects.map(({ id, text, icon }) => {
            return (
              <TouchableOpacity
                onPress={() => {
                  if (id === 0) {
                    navigation.navigate('ChoiceEmotionScreen', { emoji })
                  }
                }}
                key={id}
                style={styles.select}
              >
                <Image
                  source={icon}
                  style={{ marginRight: 8, width: 20, height: 20 }}
                  width={20}
                  height={20}
                />
                <Text style={styles.selectText}>{text}</Text>
                <Image source={check} style={{ marginLeft: 8 }} />
              </TouchableOpacity>
            )
          })}
        </ScrollView>
      </View>
    </View>
  )
}

interface Props {
  navigation: NavigationScreenProp<any, any>
  route: any
}

const styles = StyleSheet.create({
  screenWrap: {},
  selectsRowWrap: {
    position: 'absolute',
    bottom: 0,
    paddingVertical: 15,
  },
  select: {
    borderWidth: 1,
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 8,
    marginLeft: 15,
    borderColor: '#E1E3E6',
    flexDirection: 'row',
    alignItems: 'center',
  },
  selectActive: {
    borderWidth: 1,
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 8,
    marginLeft: 15,
    borderColor: theme.blueColor,
    flexDirection: 'row',
    alignItems: 'center',
  },
  selectText: {
    color: '#99a2ad',
    fontWeight: 'bold',
    fontSize: 15,
  },
})

export default PostCreationScreen

export type Emoji = {
  id: number
  name: string
  icon: any
}

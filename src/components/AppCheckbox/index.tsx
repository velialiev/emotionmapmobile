import React, { FC } from 'react'
import { StyleSheet } from 'react-native'
import { CheckBox } from 'react-native-elements'
import { useField } from 'formik'
import FontAwesome from 'react-native-vector-icons/FontAwesome'

const AppCheckBox: FC<Props> = ({
  title,
  color,
  marginBottom,
  name,
  marginTop,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  onPress,
  ...props
}) => {
  const [field, meta, helper] = useField(name)

  const hasErrors = () => {
    return meta.touched && meta.error
  }

  return (
    <>
      <CheckBox
        {...props}
        title={title}
        containerStyle={{
          borderWidth: 0,
          padding: 0,
          marginVertical: 0,
          backgroundColor: 'transparent',
          marginLeft: 0,
          marginRight: 0,
          marginBottom: marginBottom,
          marginTop: marginTop,
        }}
        checkedColor="#3f8ae0"
        textStyle={hasErrors() ? { ...styles.textStyle, color: 'red' } : styles.textStyle}
        checked={field.value}
        uncheckedColor={hasErrors() ? 'red' : 'black'}
        checkedIcon={<FontAwesome name="check-square" size={20}  />}
        onPress={(e: any) => {
          // field.onBlur(field.name)(e)
          helper.setValue(!field.value)
        }}
      />
    </>
  )
}

export default AppCheckBox

const styles = StyleSheet.create({
  textStyle: {
    fontSize: 15,
  },
})
type Props = {
  name: string
  title: string
  color: string
  marginBottom: number
  marginTop: string
  onPress: any
}

import React, { FC, useState } from 'react'
import { StyleSheet, TouchableOpacity, View } from 'react-native'
import DateTimePickerModal from 'react-native-modal-datetime-picker'
import { useField } from 'formik'
import Input from '../Input'

const DatePicker: FC<Props> = ({ name, label, placeholder, mode }) => {
  const [, , helper] = useField(name)
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false)

  const showDatePicker = () => {
    setDatePickerVisibility(true)
  }

  const hideDatePicker = () => {
    setDatePickerVisibility(false)
    helper.setTouched(true)
  }

  const handleConfirm = (date: Date) => {
    hideDatePicker()

    helper.setValue(
      mode === 'date' ? date.toLocaleDateString() : date.toLocaleTimeString().slice(0, 5),
    )
  }

  return (
    <View>
      <TouchableOpacity onPress={showDatePicker}>
        <View pointerEvents="none">
          <Input name={name} label={label} placeholder={placeholder} editable={false} />
        </View>
      </TouchableOpacity>
      <DateTimePickerModal
        isVisible={isDatePickerVisible}
        mode={mode}
        onConfirm={handleConfirm}
        onCancel={hideDatePicker}
      />
    </View>
  )
}

export default DatePicker

const styles = StyleSheet.create({
  fieldContainer: {
    borderBottomWidth: 0.5,
    height: 50,
    justifyContent: 'center',
    borderBottomColor: '#9e9e9e',
    marginTop: 20,
  },
  placeholder: {
    fontSize: 16,
    color: '#9e9e9e',
  },
  label: {
    fontSize: 16,
    color: '#9e9e9e',
    marginBottom: 5,
  },
})

type Props = {
  name: string
  label: string
  placeholder: string
  mode: Mode
  value?: string
}

type Mode = 'date' | 'time' | 'datetime'

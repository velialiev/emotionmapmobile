import React from 'react'
import { Image, StyleSheet, TouchableOpacity, View } from 'react-native'

const PlayerButtons = () => {
  return (
    <>
      <View style={styles.playerButtonWrap}>
        <TouchableOpacity>
          <Image source={require('../../assets/playerBackIcon.png')} />
        </TouchableOpacity>

        <TouchableOpacity>
          <Image source={require('../../assets/playIcon.png')} />
        </TouchableOpacity>

        <TouchableOpacity>
          <Image source={require('../../assets/playerForwardIcon.png')} />
        </TouchableOpacity>
      </View>
    </>
  )
}

export default PlayerButtons

const styles = StyleSheet.create({
  playerButtonWrap: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 80,
  },
})

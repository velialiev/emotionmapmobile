import React, { FC } from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'
import theme from '../../configs/theme'

const SoundEffect: FC<Props> = ({ imageUrl, text}) => {
  return (
    <View style={styles.wrap}>
      <View style={styles.container}>
        <Image source={imageUrl as any} />
      </View>
      <Text style={styles.text}>{text}</Text>
    </View>
  )
}

export default SoundEffect

const styles = StyleSheet.create({
  wrap: {
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 10
  },
  container: {
    width: 90,
    height: 90,
    backgroundColor: '#F5F5F5',
    borderRadius: theme.borderRadius,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 7,
  },
  text: {
    color: theme.darkColor,
    fontSize: 14
  }
})

type Props = {
  imageUrl: any
  text: string
}

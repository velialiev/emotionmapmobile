import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import ModalScreen from '../screens/ModalScreen'
import PostCreationScreen from '../screens/PostCreationScreen'
import { NavigationContainer } from '@react-navigation/native'
import EmotionMapScreen from '../screens/EmotionMapScreen'
import ChoiceEmotionScreen from '../screens/ChoiceEmotionScreen'
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import headerCheckIcon from '../assets/headerCheck.png'
import closeHeaderIcon from '../assets/closeHeader.png'
import PostsScreen from '../screens/PostsScreen'
import PostsByEmotionScreen from '../screens/PostsByEmotionScreen'

const EmotionMapStack = createStackNavigator()

const MainStackComponent = () => {
  return (
    <EmotionMapStack.Navigator initialRouteName="DonationsScreen">
      <EmotionMapStack.Screen
        name="PostCreationScreen"
        options={({ navigation }: any) => {
          return {
            headerTitle: () => {
              return (
                <View style={styles.headerTitleWrap}>
                  <Text style={styles.headerTitle}>Али</Text>
                </View>
              )
            },
            headerRight: () => {
              return (
                <TouchableOpacity onPress={() => navigation.navigate('PostsScreen')}>
                  <Image source={headerCheckIcon} style={{ marginRight: 10 }} />
                </TouchableOpacity>
              )
            },
            headerLeft: () => <Image source={closeHeaderIcon} style={{ marginRight: 10 }} />,
          }
        }}
        component={PostCreationScreen}
      />

      <EmotionMapStack.Screen
        name="ChoiceEmotionScreen"
        options={{ title: 'Выберите настроение' }}
        component={ChoiceEmotionScreen}
      />

      <EmotionMapStack.Screen name="PostsScreen" options={{ title: 'Лента' }} component={PostsScreen} />

      <EmotionMapStack.Screen
        name="PostsByEmotionScreen"
        options={({route}) => {
          return {
            title: route.params.emotion.emotionName,
          }
        }}
        component={PostsByEmotionScreen}
      />

      <EmotionMapStack.Screen
        name="EmotionMapScreen"
        options={{ title: 'Карта настроений' }}
        component={EmotionMapScreen}
      />

      <EmotionMapStack.Screen
        name="ModalScreen"
        component={ModalScreen}
        options={({ route }: any) => ({
          title: route.params?.headerTitle,
          headerBackTitle: route.params?.headerBackTitle,
        })}
      />
    </EmotionMapStack.Navigator>
  )
}

const Navigation = () => {
  return (
    <NavigationContainer>
      <MainStackComponent />
    </NavigationContainer>
  )
}

export default Navigation

const styles = StyleSheet.create({
  headerTitle: {
    fontSize: 17,
    color: 'black',
    fontWeight: '600',
  },
  headerTitleWrap: {
    flexDirection: 'row',
    alignItems: 'center',
  },
})
